<?php include('include/header.php') ?>

<main id="main">
  <article>
    <header id="header-page">
      <div class="container">
        <h1 class="h-like-c">Contactez-nous</h1>
      </div><!-- /.container -->
    </header><!-- /#header-page -->
    <div class="container">
      <div class="row pv-75 pv-50-sm pv-30-xs">
        <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
          <div class="row">
            <div class="col-md-8">
              <figure class="map-wrapper" data-aos="fade-left">
                <div id="gmap-simple" class="map-canvas"></div>
                <figcaption class="figcaption" data-aos="zoom-in-right">
                  <ul class="list">
                    <li class="item"><i class="material-icons icon">place</i>Z.I Acajou Californie <br/> 97232 Le Lamentin, Martinique</li>
                    <li class="item"><i class="material-icons icon">phone_iphone</i><a href="tel:+33596429150">Tél : 05 96 42 91 50</a></li>
                    <li class="item"><i class="material-icons icon">print</i>Fax : 05 96 42 80 43</li>
                    <li class="item"><i class="material-icons icon">schedule</i>De 9h à 18h00</li>
                  </ul>
                </figcaption><!-- .figcaption -->
              </figure> <!-- #map_wrapper -->
            </div><!-- /.col-sm-8 -->
          </div><!-- /.row -->
        </div><!-- /.col-8 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
    <section class="bg-gy-e pv-75 pv-50-sm pv-30-xs" data-aos="fade-up">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1">
            <header>
              <h2 class="h-like-c text-center">&Eacute;crivez-nous</h2>
            </header>
            <!--
            <div class="alert mt-30 mt-15-xs alert-success" role="alert">
              This is a success alert—check it out!
            </div>
            <div class="alert mt-30 mt-15-xs alert-danger" role="alert">
              This is a danger alert—check it out!
            </div>
            <div class="alert mt-30 mt-15-xs alert-warning" role="alert">
              This is a warning alert—check it out!
            </div>
            -->
            <form action="#" class="parsley-validate mt-40 mt-15-xs" data-parsley-validate novalidate>
              <ul>
                <li class="row">
                  <div class="col-sm-6">
                    <div class="input-wp">
                      <label class="label-form" for="nom">Nom</label>
                      <input type="text" class="form-control-bis" required name="nom" id="nom" placeholder="Saisissez votre nom">
                    </div><!-- .input-wp -->
                  </div><!-- .col-md-6 -->
                  <div class="col-sm-6 mt-15-xs">
                    <div class="input-wp">
                      <label class="label-form" for="prenom">Prénom</label>
                      <input type="text" class="form-control-bis" required name="prenom" id="prenom" placeholder="Saisissez votre prénom">
                    </div><!-- .input-wp -->
                  </div><!-- .col-md-6 -->
                </li><!-- .row -->
                <li class="row mt-20 mt-0-xs">
                  <div class="col-sm-6 mt-15-xs">
                    <div class="input-wp">
                      <label class="label-form" for="email">Email</label>
                      <input type="text" class="form-control-bis" required name="email" id="email" placeholder="Saisissez votre email">
                    </div><!-- .input-wp -->
                    <div class="email-error"></div>
                  </div><!-- .col-md-6 -->
                  <div class="col-sm-6 mt-15-xs">
                    <div class="input-wp">
                      <label class="label-form" for="phone">Téléphone</label>
                      <input type="number" class="form-control-bis" name="phone" id="phone" placeholder="Saisissez votre téléphone">
                    </div><!-- .input-wp -->
                  </div><!-- .col-lg-6 -->
                </li><!-- .row -->
                <li class="mt-20 mt-15-sm">
                  <div class="input-wp">
                    <label class="label-form" for="message">Message</label>
                    <textarea class="form-control-bis" id="message" rows="5" required name="message"></textarea>
                  </div><!-- .input-wp-->
                </li><!-- .row -->
              </ul>
              <div class="text-center mt-30 mt-20-xs">
                <button class="btn-wp btn-a red">Envoyer</button>
              </div>
            </form>
          </div><!-- /.col-10 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.bg-gy-a -->
  </article>
</main><!-- #main-->

<?php include('include/footer.php') ?>

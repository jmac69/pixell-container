<?php include('include/header.php') ?>

<main id="main">
  <header id="header-page">
    <div class="container">
      <h1 class="h-like-c">Liste des gabarits</h1>
    </div><!-- /.container -->
  </header><!-- /#header-page -->
  <div class="container pv-75 pv-50-md pv-30-xs">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <ul class="text-center">
          <li class="mv-10">
            <a href="home.html" class="btn-a red w-full">Page d'accueil</a>
          </li>
          <li class="mv-10">
            <a href="contact.html" class="btn-a red w-full">Page contact</a>
          </li>
          <li class="mv-10">
            <a href="listing.html" class="btn-a red w-full">Page listing</a>
          </li>
          <li class="mv-10">
            <a href="inscription.html" class="btn-a red w-full">Page inscription</a>
          </li>
        </ul>
      </div><!-- /.col-md-6 -->
    </div><!-- /.row -->
  </div><!-- /.container -->
  <main>
    <!-- #main-->

    <?php include('include/footer.php') ?>

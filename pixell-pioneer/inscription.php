<?php include('include/header.php') ?>

<main id="main">
  <article>
    <header id="header-page">
      <div class="container">
        <h1 class="h-like-c">Inscription</h1>
      </div><!-- /.container -->
    </header><!-- /#header-page -->
    <div class="container mt-50 mt-30-md">
      <div class="row">
        <div class="col-lg-10 offset-lg-1">
          <!--
          <div class="alert mt-30 mt-15-xs alert-success" role="alert">
            This is a success alert—check it out!
          </div>
          <div class="alert mt-30 mt-15-xs alert-danger" role="alert">
            This is a danger alert—check it out!
          </div>
          <div class="alert mt-30 mt-15-xs alert-warning" role="alert">
            This is a warning alert—check it out!
          </div>
          -->
          <form action="#" class="parsley-validate" data-parsley-validate novalidate>
            <section data-aos="fade-left">
              <header>
                <div class="title-icon">
                  <i class="material-icons">notes</i>
                  <h2 class="h-like-a">Vos informations</h2>
                </div>
              </header>
              <div class="form-wp mt-15">
                <ul>
                  <li class="row">
                    <div class="col-md-4">
                      <div class="input-wp">
                        <label class="label-bis" for="gender">Civilité</label>
                        <div class="selectpicker-wp">
                          <select class="selectpicker" id="gender" required title="Saisissez votre civilité" data-parsley-errors-container=".gender-error">
                            <option value="1">Mr.</option>
                            <option value="2">Mme.</option>
                            <option value="3">Mlle.</option>
                          </select><!-- .selectpicker -->
                        </div><!-- .selectpicker-wp -->
                      </div><!-- .input-wp -->
                      <div class="gender-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm">
                      <div class="input-wp">
                        <label class="label-bis" for="nom">Nom</label>
                        <input type="text" class="form-control-bis" required name="nom" id="nom" placeholder="Saisissez votre nom" data-parsley-errors-container=".nom-error">
                      </div><!-- .input-wp -->
                      <div class="nom-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm">
                      <div class="input-wp">
                        <label class="label-bis" for="prenom">Prénom</label>
                        <input type="text" class="form-control-bis" required name="prenom" id="prenom" placeholder="Saisissez votre prénom" data-parsley-errors-container=".prenom-error">
                      </div><!-- .input-wp -->
                      <div class="prenom-error"></div>
                    </div><!-- .col-md-6 -->
                  </li><!-- .row -->
                  <li class="row mt-15 mt-0-sm">
                    <div class="col-md-4 mt-15-sm ">
                      <div class="input-wp">
                        <label class="label-bis" for="email">Email</label>
                        <input type="text" class="form-control-bis" required name="email" id="email" placeholder="Saisissez votre email" data-parsley-errors-container=".email-error">
                      </div><!-- .input-wp -->
                      <div class="email-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm ">
                      <div class="input-wp">
                        <label class="label-bis" for="phone">Téléphone</label>
                        <input type="number" class="form-control-bis" required name="phone" id="phone" placeholder="Saisissez votre téléphone" data-parsley-errors-container=".phone-error">
                      </div><!-- .input-wp -->
                      <div class="phone-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm ">
                      <div class="input-wp">
                        <label class="label-bis" for="phone">Mot de passe</label>
                        <input type="password" class="form-control-bis" required name="password" id="password" placeholder="Saisissez votre mot de passe" data-parsley-errors-container=".password-error">
                      </div><!-- .input-wp -->
                      <div class="password-error"></div>
                    </div><!-- .col-md-6 -->
                  </li><!-- .row -->
                </ul>
              </div><!-- .resa-step-3 -->
            </section>
            <section data-aos="fade-right" class="mt-30">
              <header>
                <div class="title-icon">
                  <i class="material-icons">map</i>
                  <h2 class="h-like-a">Vos adresses</h2>
                </div><!-- /.title-icon -->
              </header>
              <div class="form-wp mt-15">
                <ul>
                  <li class="row">
                    <div class="col-md-12">
                      <div class="input-wp">
                        <label class="label-bis" for="adresse-1">Adresse</label>
                        <input type="text" class="form-control-bis" required name="adresse-1" id="adresse-1" placeholder="Saisissez votre prénom" data-parsley-errors-container=".adresse-1-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-1-error"></div>
                    </div><!-- .col-md-6 -->
                  </li><!-- .row -->
                  <li class="row mt-15 mt-15-sm">
                    <div class="col-md-4">
                      <div class="input-wp">
                        <label class="label-bis" for="nom">Code Postal</label>
                        <input type="text" class="form-control-bis" required name="adresse-1-cp" id="adresse-1-cp" placeholder="Saisissez votre nom" data-parsley-errors-container=".adresse-1-cp-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-1-cp-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm">
                      <div class="input-wp">
                        <label class="label-bis" for="prenom">Ville</label>
                        <input type="text" class="form-control-bis" required name="adresse-1-city" id="adresse-1-city" placeholder="Saisissez votre prénom" data-parsley-errors-container=".adresse-1-city-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-1-city-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm">
                      <div class="input-wp">
                        <label class="label-bis" for="prenom">Pays</label>
                        <input type="text" class="form-control-bis" required name="adresse-1-country" id="adresse-1-country" placeholder="Saisissez votre prénom" data-parsley-errors-container=".adresse-1-country-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-1-country-error"></div>
                    </div><!-- .col-md-6 -->
                  </li><!-- /.row -->
                  <li class="row mt-20 mt-20-sm">
                    <div class="col-md-12">
                      <div class="btn-switch-wp">
                        <div>
                          <input id="btn-switch-adress" class="checkbox-toggle checkbox-toggle-round" type="checkbox">
                          <label for="btn-switch-adress"></label>
                        </div><!-- /.radio-swtich -->
                        <div class="label">Adresse de livraison spécifique</div>
                      </div><!-- /.btn-switch-wp -->
                    </div><!-- /.col-md-4 -->
                  </li>
                </ul>
                <ul class="adresse-toggle">
                  <li class="row mt-15">
                    <div class="col-md-12">
                      <div class="input-wp">
                        <label class="label-bis" for="adresse-2">Adresse de livraison</label>
                        <input type="text" class="form-control-bis" name="adresse-2" id="adresse-2" placeholder="Saisissez votre prénom" data-parsley-errors-container=".adresse-2-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-2-error"></div>
                    </div><!-- .col-md-6 -->
                  </li><!-- .row -->
                  <li class="row mt-15">
                    <div class="col-md-4">
                      <div class="input-wp">
                        <label class="label-bis" for="nom">Code Postal de livraison</label>
                        <input type="text" class="form-control-bis" name="adresse-2-cp" id="adresse-2-cp" placeholder="Saisissez votre nom" data-parsley-errors-container=".adresse-2-cp-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-2-cp-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm">
                      <div class="input-wp">
                        <label class="label-bis" for="prenom">Ville de livraison</label>
                        <input type="text" class="form-control-bis" name="adresse-2-city" id="adresse-2-city" placeholder="Saisissez votre prénom" data-parsley-errors-container=".adresse-2-city-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-2-city-error"></div>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4 mt-15-sm">
                      <div class="input-wp">
                        <label class="label-bis" for="prenom">Pays de livraison</label>
                        <input type="text" class="form-control-bis" name="adresse-2-country" id="adresse-2-country" placeholder="Saisissez votre prénom" data-parsley-errors-container=".adresse-2-country-error">
                      </div><!-- .input-wp -->
                      <div class="adresse-2-country-error"></div>
                    </div><!-- .col-md-6 -->
                  </li><!-- /.row -->
                </ul>
              </div><!-- .resa-step-3 -->
            </section>
            <div class="btn-wp text-center mt-30 mt-20-sm" data-aos="fade">
              <button type="submit" class="btn-a red">Inscription</button>
            </div><!-- /.btn-wp -->
          </form><!-- .parsley-validate-->
        </div><!-- /.col-lg-10 -->
      </div><!-- /.row -->

    </div><!-- /.container -->
  </article>
</main><!-- #main-->

<?php include('include/footer.php') ?>

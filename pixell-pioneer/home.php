<?php include('include/header.php') ?>

<main id="main">
  <h1 class="sr-only">Distributeur n°1 des produits Pioneer dans la Caraïbe</h1>
  <section class="container mt-50 mt-30-sm" data-aos="fade-up">
    <header class="sr-only">
      <h2>Actualités mises en avant</h2>
    </header>
    <div id="slick-home-wp">
      <ul id="slick-home" class="equal-col-wp">
        <li>
          <article class="item equal-col-item" style="background-image:url('uploads/carousel-home-1.jpg')">
            <div class="content-wp">
              <header>
                <h3 class="title">La fusion du smartphone et de l’autoradio</h3>
              </header>
              <p class="desc">L'autoradio pour smartphone SPH-10BT de Pioneer * est conçu pour vous permettre d'utiliser les fonctionnalités de votre smartphone en toute sécurité, rapidement et directement.</p>
              <div class="btn-wp">
                <a href="#" class="btn-a red">Découvrir</a>
              </div>
            </div><!-- /.content -->
          </article><!-- /.item -->
        </li>
        <li>
          <article class="item equal-col-item" style="background-image:url('uploads/carousel-home-1.jpg')">
            <div class="content-wp">
              <header>
                <h3 class="title">La fusion du smartphone</h3>
              </header>
              <p class="desc">L'autoradio pour smartphone SPH-10BT de Pioneer * est conçu pour vous permettre d'améliorer vos trajets.</p>
              <div class="btn-wp">
                <a href="#" class="btn-a red">Découvrir</a>
              </div>
            </div><!-- /.content -->
          </article><!-- /.item -->
        </li><!-- /.item -->
      </ul><!-- #slick-home -->
      <div id="slick-arrows"></div>
    </div><!-- #slick-home-wp -->
  </section><!-- .container -->
  <section class="container mt-75 mt-50-md mt-30-sm">
    <header class="text-center except-zindex" data-aos="fade-down">
      <div class="h-like-a">Distributeur n°1 des produits Pioneer dans la Caraïbe</div>
    </header>
    <div class="slick-product-section pr-40 pr-0-xs mt-30 mt-15-xs" data-aos="fade-right">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-5 pr-0 pr-15-xs">
          <header class="category type-car">
            <h3 class="title">Car</h3>
            <div class="btn-wp">
              <a href="#" class="btn-b white">Tous les produits</a>
            </div><!-- .btn-wp -->
          </header><!-- .category -->
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-9 col-md-8 col-sm-7 pl-0 pl-15-xs">
          <div class="slick-product-wp type-red">
            <ul class="slick-product equal-col-wp">
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title old-price">67,40€</h5>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-car-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
            </ul><!-- /.slick-product -->
            <div class="slick-product-arrows"></div><!-- /.slick-product-arrows -->
          </div><!-- /.slick-product-wp -->
        </div><!-- /.col-lg-9 -->
      </div><!-- /.row -->
    </div><!-- /.slick-section -->
    <div class="slick-product-section pl-40 pl-0-xs mt-50 mt-30-sm" data-aos="fade-left">
      <div class="row">
        <div class="col-lg-9 col-md-8 col-sm-7 pr-0 pr-15-xs order-1 order-sm-0">
          <div class="slick-product-wp type-blue">
            <ul class="slick-product equal-col-wp">
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
            </ul><!-- /.slick-product -->
            <div class="slick-product-arrows"></div><!-- /.slick-product-arrows -->
          </div><!-- /.slick-product-wp -->
        </div><!-- /.col-lg-9 -->
        <div class="col-lg-3 col-md-4 col-sm-5 pl-0 pl-15-xs order-0 order-sm-1">
          <header class="category type-audio">
            <h3 class="title">Audio &amp; vidéo</h3>
            <div class="btn-wp">
              <a href="#" class="btn-b white">Tous les produits</a>
            </div><!-- .btn-wp -->
          </header><!-- .category -->
        </div><!-- /.col-lg-3 -->
      </div><!-- /.row -->
    </div><!-- /.slick-section -->
    <div class="slick-product-section pr-40 pr-0-xs mt-50 mt-30-sm" data-aos="fade-right">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-5 pr-0 pr-15-xs">
          <header class="category type-dj">
            <h3 class="title">DJ équipement</h3>
            <div class="btn-wp">
              <a href="#" class="btn-b white">Tous les produits</a>
            </div><!-- .btn-wp -->
          </header><!-- .category -->
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-9 col-md-8 col-sm-7 pl-0 pl-15-xs">
          <div class="slick-product-wp type-red">
            <ul class="slick-product equal-col-wp">
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-dj-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card red">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
            </ul><!-- /.slick-product -->
            <div class="slick-product-arrows"></div><!-- /.slick-product-arrows -->
          </div><!-- /.slick-product-wp -->
        </div><!-- /.col-lg-9 -->
      </div><!-- /.row -->
    </div><!-- /.slick-section -->
    <div class="slick-product-section pl-40 pl-0-xs mt-50 mt-30-sm" data-aos="fade-left">
      <div class="row">
        <div class="col-lg-9 col-md-8 col-sm-7 pr-0 pr-15-xs order-1 order-sm-0">
          <div class="slick-product-wp type-blue">
            <ul class="slick-product equal-col-wp">
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-1.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-2.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-3.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
              <li>
                <div class="item">
                  <article class="product-wp equal-col-item">
                    <figure>
                      <img src="./uploads/product-audio-4.jpg" class="w-full" alt="">
                    </figure>
                    <div class="content-wp">
                      <header class="header-wp">
                        <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System </h3>
                        <h4 class="title-sub">Réf. 984994LE</h4>
                      </header>
                      <h5 class="title">67,40€</h5>
                    </div><!-- /.content-wp -->
                    <div class="btn-wp">
                      <a href="#" class="btn-more">Détail</a>
                      <a href="#" class="btn-shopping-card blue">
                        <span class="sr-only">Ajouter au panier</span>
                      </a><!-- .btn-shopping-card -->
                    </div><!-- /.btn-wp -->
                  </article><!-- /.product-wp -->
                </div><!-- /.item -->
              </li>
            </ul><!-- /.slick-product -->
            <div class="slick-product-arrows"></div><!-- /.slick-product-arrows -->
          </div><!-- /.slick-product-wp -->
        </div><!-- /.col-lg-9 -->
        <div class="col-lg-3 col-md-4 col-sm-5 pl-0 pl-15-xs order-0 order-sm-1">
          <header class="category type-water">
            <h3 class="title">Marine audio</h3>
            <div class="btn-wp">
              <a href="#" class="btn-b white">Tous les produits</a>
            </div><!-- .btn-wp -->
          </header><!-- .category -->
        </div><!-- /.col-lg-3 -->
      </div><!-- /.row -->
    </div><!-- /.slick-section -->
  </section><!-- /.container -->
  <section id="best-sales-wp" class="mt-75 mt-50-md mt-30-sm" data-aos="fade">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-2">
          <header data-aos="fade-right">
            <h2 class="title-section">Nos meilleures ventes</h2>
          </header>
        </div><!-- /.col-md-3 -->
        <div class="col-lg-10 pl-50-lg pl-15-md">
          <div class="row">
            <div class="col mt-30-md" data-aos-delay="200" data-aos="fade-up">
              <article class="product-wp">
                <figure>
                  <img src="./uploads/product-dj-1.jpg" class="w-full" alt="">
                </figure>
                <div class="content-wp">
                  <header class="header-wp">
                    <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                    <h4 class="title-sub">Réf. 984994LE</h4>
                  </header>
                  <h5 class="title">67,40€</h5>
                </div><!-- /.content-wp -->
                <div class="btn-wp">
                  <a href="#" class="btn-more">Détail</a>
                  <a href="#" class="btn-shopping-card red">
                    <span class="sr-only">Ajouter au panier</span>
                  </a><!-- .btn-shopping-card -->
                </div><!-- /.btn-wp -->
              </article><!-- /.product-wp -->
            </div><!-- /.col -->
            <div class="col mt-30-md" data-aos-delay="400" data-aos="fade-down">
              <article class="product-wp">
                <figure>
                  <img src="./uploads/product-dj-2.jpg" class="w-full" alt="">
                </figure>
                <div class="content-wp">
                  <header class="header-wp">
                    <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System</h3>
                    <h4 class="title-sub">Réf. 984994LE</h4>
                  </header>
                  <h5 class="title">67,40€</h5>
                </div><!-- /.content-wp -->
                <div class="btn-wp">
                  <a href="#" class="btn-more">Détail</a>
                  <a href="#" class="btn-shopping-card red">
                    <span class="sr-only">Ajouter au panier</span>
                  </a><!-- .btn-shopping-card -->
                </div><!-- /.btn-wp -->
              </article><!-- /.product-wp -->
            </div><!-- /.col -->
            <div class="w-full d-block d-md-none"></div>
            <div class="col mt-30-md" data-aos-delay="600" data-aos="fade-up">
              <article class="product-wp">
                <figure>
                  <img src="./uploads/product-dj-3.jpg" class="w-full" alt="">
                </figure>
                <div class="content-wp">
                  <header class="header-wp">
                    <h3 class="title">Pioneer XDJ-RR</h3>
                    <h4 class="title-sub">Réf. 984994LE</h4>
                  </header>
                  <h5 class="title">67,40€</h5>
                </div><!-- /.content-wp -->
                <div class="btn-wp">
                  <a href="#" class="btn-more">Détail</a>
                  <a href="#" class="btn-shopping-card red">
                    <span class="sr-only">Ajouter au panier</span>
                  </a><!-- .btn-shopping-card -->
                </div><!-- /.btn-wp -->
              </article><!-- /.product-wp -->
            </div><!-- /.col -->
            <div class="col mt-30-md" data-aos-delay="800" data-aos="fade-down">
              <article class="product-wp">
                <figure>
                  <img src="./uploads/product-dj-4.jpg" class="w-full" alt="">
                </figure>
                <div class="content-wp">
                  <header class="header-wp">
                    <h3 class="title">Pioneer XDJ-RR All-in-one</h3>
                    <h4 class="title-sub">Réf. 984994LE</h4>
                  </header>
                  <h5 class="title">67,40€</h5>
                </div><!-- /.content-wp -->
                <div class="btn-wp">
                  <a href="#" class="btn-more">Détail</a>
                  <a href="#" class="btn-shopping-card red">
                    <span class="sr-only">Ajouter au panier</span>
                  </a><!-- .btn-shopping-card -->
                </div><!-- /.btn-wp -->
              </article><!-- /.product-wp -->
            </div><!-- /.col -->
            <div class="col d-none d-xl-block" data-aos-delay="1000" data-aos="fade-up">
              <article class="product-wp">
                <figure>
                  <img src="./uploads/product-car-1.jpg" class="w-full" alt="">
                </figure>
                <div class="content-wp">
                  <header class="header-wp">
                    <h3 class="title">Pioneer XDJ-RR</h3>
                    <h4 class="title-sub">Réf. 984994LE</h4>
                  </header>
                  <h5 class="title">67,40€</h5>
                </div><!-- /.content-wp -->
                <div class="btn-wp">
                  <a href="#" class="btn-more">Détail</a>
                  <a href="#" class="btn-shopping-card red">
                    <span class="sr-only">Ajouter au panier</span>
                  </a><!-- .btn-shopping-card -->
                </div><!-- /.btn-wp -->
              </article><!-- /.product-wp -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.col-md-9 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section><!-- #best-sales-wp -->
  <section class="container mt-75 mt-50-md mt-30-sm">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">
        <header data-aos="fade-right">
          <h2 class="h-like-a">Distributeur n°1 des produits Pioneer dans la Caraïbe</h2>
        </header>
        <div class="mt-15" data-aos="fade-left">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est debitis quae voluptatem non totam repudiandae nobis velit sint sed inventore odio assumenda facere iusto, ducimus nam magni rerum aliquam ab!</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex tempore rerum porro totam nisi nulla accusamus repudiandae quaerat eum eos distinctio, optio similique reiciendis minus? Sunt blanditiis esse veniam soluta laborum dolorum
            vero dignissimos saepe deserunt aspernatur, asperiores ducimus, fugiat nostrum quisquam provident odit, natus. Qui.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis nihil animi odio voluptate incidunt dolores dignissimos deserunt ad.</p>
        </div>
      </div><!-- /.col-md-10 -->
    </div><!-- /.row -->
  </section><!-- /.container -->
</main><!-- #main-->

<?php include('include/footer.php') ?>

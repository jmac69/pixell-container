<footer id="footer" class="d-print-none">
  <div id="footer-top">
    <div class="container">
      <h3 class="h-like-a big">En savoir +</h3>
      <div class="row mt-15">
        <div class="col-lg-3 col-md-4 col-sm-6">
          <ul class="list-link">
            <li class="item">
              <a class="link">Conditions générales de vente</a>
            </li><!-- .item -->
            <li class="item">
              <a class="link">Mentions légales</a>
            </li><!-- .item -->
            <li class="item">
              <a class="link">Plan du site</a>
            </li><!-- .item -->
          </ul><!-- .list -->
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-3 col-md-4 col-sm-6 mt-15-xs">
          <ul class="list">
            <li class="item">3 boulevard des Habissois Souverains<br />97 119 Vieux Habitants</li>
            <li class="item"><a href="tel:+33596419150">Téléphone : 05 96 41 91 50</a></li>
            <li class="item">Fax : 05 96 42 80 43</li>
          </ul><!-- /.list -->
        </div><!-- /.col-lg-3 -->
        <div class="col-lg-6 col-md-4 pl-40 pl-15-md d-none d-md-block">
          <a href="#" class="logo-wp">
            <img src="theme/images/logos/pioneer-antilles-logo-black.png" class="img-fluid" width="300" alt="Logotype Pioneer">
          </a>
        </div><!-- /.col-md-4 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /#footer-top -->
  <div id="footer-bottom" class="mt-30">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4 col-sm-6">
          <ul class="list-inline">
            <li>
              <a href="#" class="link">Politique de confidentialité</a>
            </li>
            <li>
              <a href="#" class="link">Mentions légales</a>
            </li>
          </ul>
        </div><!-- /.col-md-4 -->
        <div class="col-sm-6 text-center text-sm-right text-md-center mt-10-xs">
          <a href="http://www.pixellweb.com/" target="_blank" rel="nofollow" class="logo">
            <span>Référencé, réalisé et hébergé par Pixell</span>
            <img src="theme/images/logos/pixell-logo.png" width="25" alt="Logo Pixell">
          </a>
        </div><!-- /.col-md-4 -->
      </div><!-- /.row -->
    </div> <!-- .container -->
  </div><!-- /#footer-bottom -->
</footer><!-- #footer -->

<div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    </div><!-- .modal-content -->
  </div><!-- .modal-dialog -->
</div><!-- .modal -->

<a href="#" title="Retour en haut de page" id="btn-back-top">
  <span class="sprite back-top"></span>
</a><!-- #btn-top -->

</div><!-- .ie-wp -->

<div id="modal-loader">
  <div class="spinner-wp spinner-circle"></div>
</div><!-- #modal-loader -->

<script src="theme/dist/index.js"></script>

</body>

</html>

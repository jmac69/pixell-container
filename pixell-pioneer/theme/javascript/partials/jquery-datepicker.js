// JS dependency
var datepicker = require('jquery-ui/ui/widgets/datepicker');
var lang = $('body').attr('data-lang');
if(lang=='fr'){
  var datepicker_fr = require('jquery-ui/ui/i18n/datepicker-fr.js');
}

// CSS dependencies
import '/../node_modules/jquery-ui/themes/base/theme.css';
import '/../node_modules/jquery-ui/themes/base/datepicker.css';

// Input Start
$("#datepicker-start").datepicker({
  regional: lang,
  showOn: "both",
  buttonText: "<i class='material-icons'>calendar_today</i>",
  //buttonImage: 'theme/images/icons/gmap-marker.png',
  buttonImageOnly: false,
  defaultDate: "+1w",
  numberOfMonths: 1,
  minDate: 0,
  maxDate: '1Y',
  onClose: function(selectedDate) {
    $("#datepicker-end").datepicker("option", "minDate", selectedDate);
  }
});

// Input End
$("#datepicker-end").datepicker({
  regional: lang,
  showOn: "both",
  buttonText: "<i class='material-icons'>calendar_today</i>",
  //buttonImage: 'theme/images/icons/gmap-marker.png',
  buttonImageOnly: false,
  defaultDate: "+1w",
  maxDate: '1Y',
  numberOfMonths: 1,
  onClose: function(selectedDate) {
    $("#datepicker-start").datepicker("option", "maxDate", selectedDate);
  }
});

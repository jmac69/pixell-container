// JS dependency
var slick = require('slick-carousel');

// CSS dependency
import '/../node_modules/slick-carousel/slick/slick.css';

// Slick :: Home top
$('#slick-home').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  fade: true,
  arrows: false,
  autoplay: true,
  adaptiveHeight: false,
  cssEase: 'ease-in-out',
  speed: 400,
  autoplaySpeed: 6000,
  // appendArrows: '#slick-arrows',
  // prevArrow: '<button class="arrow-prev"><i class="material-icons">arrow_backward</i></button>',
  // nextArrow: '<button class="arrow-next"><i class="material-icons">arrow_forward</i></button>',
  responsive: [{
      breakpoint: 1200,
      settings: {}
    },
    {
      breakpoint: 767,
      settings: {
        adaptiveHeight: true,
        autoplay: false,
      }
    },
    {
      breakpoint: 576,
      settings: {
        adaptiveHeight: true,
        autoplay: false,
      }
    },
  ]
});

setTimeout(function() {
  $('#slick-home .slick-active').addClass('animation');
}, 250);

$('#slick-home').on('afterChange', function(event, slick, currentSlide) {
  $('#slick-home .slick-active').addClass('animation');
});

$('#slick-home').on('beforeChange', function(event, slick, currentSlide) {
  $('#slick-home .slick-active').removeClass('animation');
});

// Slick :: Product generic
// Slick generic
var i = 0;
var delay = 4000;
var delayIncrement;
$('.slick-product-wp').each(function() {
  i++;
  delayIncrement = delay + (250 * i);
  $(this).find('.slick-product').attr('id', 'slick-product-' + i);
  $(this).find('.slick-product-arrows').attr('id', 'slick-product-arrows-' + i);
  $('#slick-product-' + i).slick({
    slidesToShow: 4,
    slidesToScroll: 2,
    dots: false,
    fade: false,
    arrows: true,
    autoplay: true,
    infinite: true,
    adaptiveHeight: true,
    cssEase: 'ease-in-out',
    pauseOnHover: true,
    speed: 400,
    autoplaySpeed: delayIncrement,
    appendArrows: '#slick-product-arrows-' + i,
    prevArrow: '<button class="slick-prev"><span class="sprite"></span></button>',
    nextArrow: '<button class="slick-next"><span class="sprite"></span></button>',
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true
        }
      }
    ]
  });
});

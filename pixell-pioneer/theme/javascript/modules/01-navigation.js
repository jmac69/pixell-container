// Navigation toggle Search
function nav_search(){

  var ariaExpanded;
  var viewport = $(window).width();
  var target = $('#header-top .link-wp[data-toggle-search]');

  function nav_search_action (){
    target.on('click', function (){
      var isExpanded = $('#search-toggle').hasClass('open');
      isExpanded ? ariaExpanded = true : ariaExpanded = false;
      $(this).attr('aria-expanded', ariaExpanded);
      $('#search-toggle').toggle().toggleClass('open');
    });
  }

  function nav_search_mobile(){
    var viewport = $(window).width();
    if( viewport > 565 && viewport < 768) {
      nav_search_action();
      target.trigger('click');
    }
  }

  nav_search_mobile();
  $(window).on('resize', function() {
    nav_search_mobile();
  });
}

// Subscribe
function f_subscribe(){
  var isChecked = $('#btn-switch').prop('checked');
  if(!isChecked){
    $('.input-hide').hide();
  }else{
    $('.input-hide input').attr('required','required');
  }
  $('#btn-switch').on('change',function(){
    $('.input-hide').toggle();
    $('.input-hide').toggleClass('active');
    if($('.input-hide').hasClass('active')){
      $('.input-hide input').attr('required','required');
    }else{
      $('.input-hide input').removeAttr('required')
    }

  })
  $('.adresse-toggle').hide();
  var isCheckedAdress = $('#btn-switch-adress').prop('checked');
  if(isCheckedAdress){
    $('.adresse-toggle').show();
    $('.adresse-toggle').addClass('active');
    $('.adresse-toggle .form-control-bis').attr('required','required')
  }

  $('#btn-switch-adress').on('change',function(){
    $('.adresse-toggle').toggle();
    $('.adresse-toggle').toggleClass('active');
    if($('.adresse-toggle').hasClass('active')){
      $('.adresse-toggle .form-control-bis').attr('required','required')
    }else{
      $('.adresse-toggle .form-control-bis').removeAttr('required')
    }
  })

}


// Navigation mobile
function nav_mobile() {
  var ariaExpanded;
  $('#nav-mobile-trigger').on('click', function() {
    $('body').toggleClass('menu-open');
    $('#header-bottom').toggle();
    var isExpanded = $('body').hasClass('menu-open');
    isExpanded ?  ariaExpanded = true : ariaExpanded = false;
    $(this).attr('aria-expanded', ariaExpanded);
  });
}

// Navigation size
function nav_size() {
  var position = $(window).scrollTop();
  var header = $('#header').innerHeight();
  var headerTop = $('#header-top').innerHeight();
  if (position > headerTop) {
    $('body')
      .addClass('header-small')
      .css('paddingTop',header);
  } else {
    $('body')
      .removeClass('header-small')
      .css('paddingTop',0);
  }
  $(window).scroll(function() {
    if ($(this).scrollTop() > headerTop) {
      $('body')
        .addClass('header-small')
        .css('paddingTop', header);
    } else {
      $('body')
        .removeClass('header-small')
        .css('paddingTop',0);
    }
  });
}

// Navigation sub menu
function nav_sub_menu() {
  var viewport = $(window).width();
  if (viewport < 576) {
    $('#nav-main li[data-navsub] > a').on('click', function(event) {
      var navSub = $(this).parent();
      var isFocus = navSub.hasClass('focus');
      $('#nav-main li[data-navsub]').removeClass('focus');
      if (!isFocus) {
        navSub.addClass('focus');
      }
      return false;
    });
  } else {
    $('#nav-main li[data-navsub] > a').on('click', function(event) {
      $('#search-toggle').hide();
      var navSub = $(this).parent();
      var isFocus = navSub.hasClass('focus');
      $('#nav-main li[data-navsub]').removeClass('focus');
      $('body').removeClass('navsub-open');
      if (!isFocus) {
        navSub.addClass('focus');
        $('body').addClass('navsub-open');
      }
      event.stopPropagation();
      return false;
    });
    $('html').click(function() {
      $('#nav-main li[data-navsub]').removeClass('focus');
      $('body').removeClass('navsub-open');
    });
  }
}

// Scroll top button
function scroll_top() {
  $('#btn-back-top').fadeOut(0);
  $(window).scroll(function() {
    if ($(this).scrollTop() > 220) {
      $('#btn-back-top').fadeIn(300);
    } else {
      $('#btn-back-top').fadeOut(300);
    }
  });
  $('#btn-back-top').on('click', function() {
    $('html, body').animate({
      scrollTop: 0
    }, '500');
    return false;
  });
}

// Initialisation
nav_search();
nav_mobile();
nav_sub_menu();
nav_size();
scroll_top();
f_subscribe();

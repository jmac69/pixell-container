<?php include('include/header.php') ?>

<main id="main">
  <header id="header-page">
    <div class="container">
      <h1 class="h-like-c">Tous les produits Dj équipement</h1>
    </div><!-- /.container -->
  </header><!-- /#header-page -->
  <div class="container mt-75 mt-50-md mt-30-sm mb-50 mb-0-sm">
    <div class="row">
      <div class="col-lg-3 col-md-4">
        <div class="category-filter accordion" id="sticky-wp">
          <!--
            Car : category-car
            Audio : category-audio
            Dj équipement : category-dj
            Water : category-water
          -->
          <header class="title-wp category-dj">
            <h2 class="title">Dj équipement</h2>
            <div class="btn-wp">
              <button class="btn-b white" id="trigger-filter-content">Filtrer par catégorie</button>
            </div><!-- /.btn-wp -->
          </header><!-- /.title-wp -->
          <ul class="content">
            <li class="content-item">
              <a href="#" class="title-sub">Cheveux naturels<i class="material-icons">chevron_right</i></a>
              <ul class="list-sub">
                <li class="item-sub">
                  <a href="#" class="link-sub">Cheveux Naturels indien</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Cheveux Naturels Brésilien</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Cheveux Malaisien</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Cheveux Péruvien</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Cheveux Chinois</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Tissage</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Bouclé</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Lisse</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Ondulé</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Crépu</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Yaki</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Tressée</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub except">Tout voir</a>
                </li><!-- /.item-sub -->
              </ul>
            </li>
            <li class="content-item">
              <a href="#" class="title-sub opened">Cheveux synthétiques <i class="material-icons">chevron_right</i></a>
              <ul class="list-sub">
                <li class="item-sub">
                  <a href="#" class="link-sub">Extra courte</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Courte</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub active">Longueur de menton</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Longueur d'épaule</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Longue</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Tissage</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Bouclé</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Lisse</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Ondulé</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Crépu</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Yaki</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Tressée</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub except">Tout voir</a>
                </li><!-- /.item-sub -->
              </ul>
            </li>
            <li class="content-item">
              <a href="#" class="title-sub">Perruques Laces (Dentelles) <i class="material-icons">chevron_right</i></a>
              <ul class="list-sub">
                <li class="item-sub">
                  <a href="#" class="link-sub">Perruques Lace Front</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Perruques Full Lace</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Perruques Monofilament</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Perruques 100% fait à la main</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Perruques laque non glu</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Perruques Lace Célébrités</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Perruques Lace en cheveux naturels Rémy</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Tresse</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Bouclé</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Lisse</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Ondulé</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Crépu</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Yaki</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Tressée</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub except">Tout voir</a>
                </li><!-- /.item-sub -->
              </ul>
            </li>
            <li class="content-item">
              <a href="#" class="title-sub">Mèches <i class="material-icons">chevron_right</i></a>
              <ul class="list-sub">
                <li class="item-sub">
                  <a href="#" class="link-sub">Mèches pour tresses</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Mèches pour vanilles et twist</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Mèches pour crochet braid</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub except">Tout voir</a>
                </li><!-- /.item-sub -->
              </ul>
            </li>
            <li class="content-item">
              <a href="#" class="title-sub">Extensions <i class="material-icons">chevron_right</i></a>
              <ul class="list-sub">
                <li class="item-sub">
                  <a href="#" class="link-sub">Extensions à clip pour tresses</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Extensions de tissage</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Onglue/U Extensions de cheveux</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Onglue/U Extensions de cheveux</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Onglue/U Extensions de cheveux</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Stick/I Extensions de cheveux</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Cercle Micro extensions de cheveux</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Extensions enclaver dans les cheveux</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Fermeture de Lace</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Lace Frontales</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Nattes/tresses</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub except">Tout voir</a>
                </li><!-- /.item-sub -->
              </ul>
            </li>
            <li class="content-item">
              <a href="#" class="title-sub">Postiches <i class="material-icons">chevron_right</i></a>
              <ul class="list-sub">
                <li class="item-sub">
                  <a href="#" class="link-sub">Postiches à clip</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Queue de Cheval</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Postiches en cheveux Humains</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Les cheveux Tmber en Demi-tête</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Frange à Clip</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub">Chignons</a>
                </li><!-- /.item-sub -->
                <li class="item-sub">
                  <a href="#" class="link-sub except">Tout voir</a>
                </li><!-- /.item-sub -->
              </ul>
            </li>
            <li class="content-item">
              <a href="#" class="title-sub no-child">Marque <i class="material-icons">chevron_right</i></a>
            </li>
          </ul><!-- .content -->
        </div><!-- .category-filter-->
      </div><!-- /.col-md-4 -->
      <div class="col-lg-9 col-md-8 pl-50 pl-30-md pl-15-sm mt-30-sm mt-15-xs">
        <div class="row">
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-car-1.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title old-price">67,40€</h5>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-car-1.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-car-2.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-car-3.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR A</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-dj-1.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-dj-2.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-dj-3.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-sm-6 mb-30 mb-15-xs" data-aos="zoom-in">
            <article class="product-wp listing">
              <figure>
                <img src="./uploads/product-dj-4.jpg" class="w-full" alt="">
              </figure>
              <div class="content-wp">
                <header class="header-wp">
                  <h3 class="title">Pioneer XDJ-RR All-in-one DJ-System pour rekordbox</h3>
                  <h4 class="title-sub">Réf. 984994LE</h4>
                </header>
                <h5 class="title">67,40€</h5>
              </div><!-- /.content-wp -->
              <div class="btn-wp">
                <a href="#" class="btn-more">Détail</a>
                <a href="#" class="btn-shopping-card red">
                  <span class="sr-only">Ajouter au panier</span>
                </a><!-- .btn-shopping-card -->
              </div><!-- /.btn-wp -->
            </article><!-- /.product-wp -->
          </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
        <div class="pagination-wp">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li><!-- /.page-item -->
            <li class="page-item">
              <a class="page-link" href="#">1</a>
            </li><!-- /.page-item -->
            <li class="page-item active">
              <a class="page-link" href="#">2</a>
            </li><!-- /.page-item -->
            <li class="page-item">
              <a class="page-link" href="#">3</a>
            </li><!-- /.page-item -->
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li><!-- /.page-item -->
          </ul><!-- /.pagination -->
        </div>
      </div><!-- /.col-md-8 -->
    </div>
  </div><!-- /.container -->
</main><!-- #main-->

<?php include('include/footer.php') ?>

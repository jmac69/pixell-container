<?php include('include/header.php') ?>

<main id="main">
  <div class="container mv-100">
    <header>
      <h1>Component : list</h1>
    </header>
    <section class="mt-50">
      <header>
        <h2>Component : Form</h2>
      </header>
      <div class="alert alert-success" role="alert">
        This is a success alert—check it out!
      </div>
      <div class="alert alert-danger" role="alert">
        This is a danger alert—check it out!
      </div>
      <div class="alert alert-warning" role="alert">
        This is a warning alert—check it out!
      </div>
      <script>
      </script>
      <form action="#" class="parsley-validate" data-parsley-validate novalidate>
        <ul>
          <li class="mt-15">
            <input type="text" class="form-control" placeholder="Nom *" required>
          </li>
          <li class="mt-15">
            <input type="email" class="form-control" placeholder="Email *" required>
          </li>
          <li class="mt-15">
            <input type="tel" class="form-control placeholder" placeholder="Téléphone *" required>
          </li>
          <li class="mt-15">
            <div class="datepicker-wp">
              <input type="text" class="form-control" id="datepicker-start" placeholder="Date de départ" data-parsley-errors-container=".datepicker-start-error" required autocomplete="off">
            </div><!-- /.datepicker-wp -->
            <div class="datepicker-start-error"></div>
          </li>
          <li class="mt-15">
            <div class="datepicker-wp">
              <input type="text" class="form-control" id="datepicker-end" placeholder="Date d'arrivée" data-parsley-errors-container=".datepicker-end-error" required autocomplete="off">
            </div><!-- /.datepicker-wp -->
            <div class="datepicker-end-error"></div>
          </li>
          <li class="mt-15">
            <div class="btn-switch-wp">
              <div>
                <input id="btn-switch" class="checkbox-toggle checkbox-toggle-round" type="checkbox" data-parsley-errors-container=".btn-switch-error" required>
                <label for="btn-switch"></label>
              </div><!-- /.radio-swtich -->
              <div class="label">Bouton radio ON/OFF *</div>
            </div><!-- /.btn-switch-wp -->
            <div class="btn-switch-error"></div>
          </li>
          <li class="mt-15">
            <div class="range-wp">
              <input id="range" type="text" value="" data-slider-min="5000" data-slider-max="30000" data-slider-step="5" data-slider-value="[5000,30000]" />
              <div class="value-wp">
                <span class="value min"><span class="txt">5000</span> €</span>
                <span class="value max"><span class="txt">30000</span> €</span>
              </div><!-- /.info-wp -->
            </div><!-- /.range-wp -->
          </li>
          <li class="mt-15">
            <div class="selectpicker-wp">
              <select class="selectpicker" id="test" title="Lorem ipsum dolor sit amet," data-parsley-errors-container=".select-error" required>
                <option value="1">Lorem pyag ipsum 1</option>
                <option value="2">Lorem ipsum 2</option>
                <option value="3">Lorem ipsum 3</option>
                <option value="4">Lorem ipsum 4</option>
                <option value="5">Lorem ipsum 5</option>
                <option value="2">Lorem ipsum 2</option>
                <option value="3">Lorem ipsum 3</option>
                <option value="4">Lorem ipsum 4</option>
                <option value="5">Lorem ipsum 5</option>
                <option value="2">Lorem ipsum 2</option>
                <option value="3">Lorem ipsum 3</option>
                <option value="4">Lorem ipsum 4</option>
                <option value="5">Lorem ipsum 5</option>
                <option value="2">Lorem ipsum 2</option>
                <option value="3">Lorem ipsum 3</option>
                <option value="4">Lorem ipsum 4</option>
                <option value="5">Lorem ipsum 5</option>
                <option value="2">Lorem ipsum 2</option>
                <option value="3">Lorem ipsum 3</option>
                <option value="4">Lorem ipsum 4</option>
                <option value="5">Lorem ipsum 5</option>
                <option value="2">Lorem ipsum 2</option>
                <option value="3">Lorem ipsum 3</option>
                <option value="4">Lorem ipsum 4</option>
                <option value="5">Lorem ipsum 5</option>
                <option value="2">Lorem ipsum 2</option>
                <option value="3">Lorem ipsum 3</option>
                <option value="4">Lorem ipsum 4</option>
                <option value="5">Lorem ipsum 5</option>
              </select><!-- /.selectpicker -->
            </div><!-- /.selectpicker-wp -->
            <div class="select-error"></div>
          </li>
          <li class="mt-15">
            <div class="custom-file-wp">
              <input type="file" class="custom-file-input" id="custom-file" data-parsley-errors-container=".file-error" required>
              <label class="custom-file-label" for="custom-file">Sélectionner un fichier</label>
            </div><!-- /.custom-file-wp -->
            <div class="file-error"></div>
          </li>
          <li class="mt-15">
            <div class="checkboxradio-wp">
              <input type="checkbox" name="checkbox" id="checkbox" data-parsley-errors-container=".checkbox-error" required>
              <label for="checkbox">Chekbox</label>
            </div><!-- /.checkboxradio-wp -->
            <div class="checkbox-error"></div>
          </li>
          <li class="mt-15">
            <div class="checkboxradio-wp">
              <input type="radio" name="radio-array" id="radio-1" data-parsley-errors-container=".radio-error" required>
              <label for="radio-1">Radio 1 </label>
            </div><!-- /.checkboxradio-wp -->
            <div class="checkboxradio-wp">
              <input type="radio" name="radio-array" id="radio-2">
              <label for="radio-2">Radio 1 </label>
            </div><!-- /.checkboxradio-wp -->
            <div class="radio-error"></div>
          </li>
          <li class="mt-15">
            <textarea rows="5" class="form-control" placeholder="Message *" required></textarea>
          </li>
          <li class="mt-15">
            <button type="submit">Envoyer</button>
          </li>
        </ul>
      </form>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Breadcrumb</h2>
      </header>
      <nav aria-label="breadcrumb">
        <h3 class="sr-only">breadcrumb</h3>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Library</a></li>
          <li class="breadcrumb-item active" aria-current="page">Data</li>
        </ol><!-- /.breadcrumb -->
      </nav>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Slick</h2>
      </header>
      <div id="slick-wp">
        <ul id="slick">
          <li>
            <div class="item">
              <figure class="content">
                <img src="http://placehold.it/1200x750" class="img-fluid" alt="">
              </figure>
            </div>
          </li><!-- /.item -->
          <li>
            <div class="item">
              <figure class="content">
                <img src="http://placehold.it/1200x750" class="img-fluid" alt="">
              </figure>
            </div>
          </li><!-- /.item -->
          <li>
            <div class="item">
              <figure class="content">
                <img src="http://placehold.it/1200x750" class="img-fluid" alt="">
              </figure>
            </div>
          </li><!-- /.item -->
          <li>
            <div class="item">
              <figure class="content">
                <img src="http://placehold.it/1200x750" class="img-fluid" alt="">
              </figure>
            </div>
          </li><!-- /.item -->
          <li>
            <div class="item">
              <figure class="content">
                <img src="http://placehold.it/1200x750" class="img-fluid" alt="">
              </figure>
            </div>
          </li><!-- /.item -->
        </ul>
        <div id="slick-arrows"></div>
      </div>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Paginate</h2>
      </header>
      <nav aria-label="Page navigation example">
        <h3 class="sr-only">Page navigation example</h3>
        <ul class="pagination">
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li><!-- /.page-item -->
          <li class="page-item">
            <a class="page-link" href="#">1</a>
          </li><!-- /.page-item -->
          <li class="page-item">
            <a class="page-link" href="#">2</a>
          </li><!-- /.page-item -->
          <li class="page-item">
            <a class="page-link" href="#">3</a>
          </li><!-- /.page-item -->
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li><!-- /.page-item -->
        </ul><!-- /.pagination -->
      </nav>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Paralax</h2>
      </header>
      <div class="parallax-wp" data-image-src="http://placehold.it/1200x750">
        <img src="http://placehold.it/1200x750" class="sr-only" alt="">
      </div><!-- /.parallax-wp -->
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Google Map Single</h2>
      </header>
      <figure class="map-wrapper">
        <div id="gmap-simple" class="map-canvas"></div>
      </figure> <!-- /#map_wrapper -->
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Google Map Multiple</h2>
      </header>
      <figure class="map-wrapper">
        <div id="gmap-cluster" class="map-canvas"></div>
      </figure> <!-- /#map_wrapper -->
      <script>
        var locations = [
          {
            lat: 14.619895,
            lng: -61.012931,
            info: "<div></div>"
          },
          {
            lat: 14.621500,
            lng: -60.991672,
            info: "<div></div>"
          },{
            lat: 16.240886,
            lng: -61.583154,
            info: "<div></div>"
          },
          {
            lat: 16.240039,
            lng: -61.579356,
            info: "<div></div>"
          },
          {
            lat: 18.108137,
            lng: -63.052557,
            info: "<div></div>"
          },
          {
            lat: 4.921299,
            lng: -52.293152,
            info: "<div></div>"
          },
          {
            lat: 4.920791,
            lng: -52.293538,
            info: "<div></div>"
          },
          {
            lat: 44.868950,
            lng:  -0.547877,
            info: "<div></div>"
          }
      ];
      </script>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Collapse</h2>
      </header>
      <div>
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Link with href</a>
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Button with data-target</button>
      </div>
      <div class="collapse" id="collapseExample">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</div>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Modal</h2>
      </header>
      <a href="modal.html" class="trigger-modal">Trigger basic - </a>
      <a href="modal.html" class="trigger-modal" data-align="modal-dialog-centered" data-size="modal-lg">Trigger Centered and large - </a>
      <a href="modal.html" class="trigger-modal" data-align="modal-dialog-centered" data-size="modal-sm">Trigger Centered and small - </a>
      <a href="modal.html" class="trigger-modal" data-align="modal-dialog-centered">Trigger Centered normal </a>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Dropdown</h2>
      </header>
      <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown button</button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div><!-- /.dropdown-menu -->
      </div><!-- /.dropdown -->
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Tooltip</h2>
      </header>
      <button type="button" class="btn" data-toggle="tooltip" data-placement="top" title="Tooltip on top">Tooltip on top</button>
      <button type="button" class="btn" data-toggle="tooltip" data-placement="right" title="Tooltip on top">Tooltip on right</button>
      <button type="button" class="btn" data-toggle="tooltip" data-placement="bottom" title="Tooltip on top">Tooltip on bottom</button>
      <button type="button" class="btn" data-toggle="tooltip" data-placement="left" title="Tooltip on top">Tooltip on left</button>
      <!-- /Generated markup by the plugin -->
      <div class="tooltip bs-tooltip-top d-none" role="tooltip">
        <div class="arrow"></div>
        <div class="tooltip-inner">Some tooltip text!</div>
      </div>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Popover</h2>
      </header>
      <button type="button" class="btn" data-toggle="popover" data-placement="top" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?" data-template="<div class='popover' role='tooltip'><div class='arrow'></div><h3 class='popover-header'></h3><div class='popover-body'></div></div>'">Popover
        Top</button>
      <button type="button" class="btn" data-toggle="popover" data-placement="right" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?" data-template="<div class='popover' role='tooltip'><div class='arrow'></div><h3 class='popover-header'></h3><div class='popover-body'></div></div>'">Popover
        Right</button>
      <button type="button" class="btn" data-toggle="popover" data-placement="bottom" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?" data-template="<div class='popover' role='tooltip'><div class='arrow'></div><h3 class='popover-header'></h3><div class='popover-body'></div></div>'">Popover
        Bottom</button>
      <button type="button" class="btn" data-toggle="popover" data-placement="left" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?" data-template="<div class='popover' role='tooltip'><div class='arrow'></div><h3 class='popover-header'></h3><div class='popover-body'></div></div>'">Popover
        Left</button>
    </section>

    <section class="mt-50">
      <header>
        <h2>Component : Accordion</h2>
      </header>
      <div id="accordion">
        <div id="headingOne">
          <button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Collapsible Group Item #1</button>
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi et expedita ea beatae natus cum, dolores nam consequatur fugiat in.</p>
          </div><!-- /.collapse -->
        </div>
        <div id="headingTwo">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Collapsible Group Item #2</button>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi et expedita ea beatae natus cum, dolores nam consequatur fugiat in.</p>
          </div><!-- /.collapse -->
        </div><!-- /#headingTwo -->
        <div id="headingThree">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Collapsible Group Item #2</button>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi et expedita ea beatae natus cum, dolores nam consequatur fugiat in.</p>
          </div><!-- /.collapse -->
        </div><!-- /#headingTwo -->
      </div><!-- /#accordion -->
    </section>

    <section class="mv-50">
      <header>
        <h2>Component : Magnific Popup</h2>
      </header>
      <div class="magnific-popup">
        <ul class="row">
          <li class="col">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum" class="item mfp-iframe" data-effect="mfp-zoom-out">
              <img src="http://placehold.it/350x150" class="img-fluid w-100" alt="#">
            </a><!-- /.item -->
          </li><!-- /.col -->
          <li class="col">
            <a href="http://placehold.it/350x150" title="Lorem ipsum" class="item" data-effect="mfp-zoom-out">
              <img src="http://placehold.it/350x150" class="img-fluid w-100" alt="#">
            </a><!-- /.item -->
          </li><!-- /.col -->
        </ul><!-- /.row -->
      </div><!-- /.magnific-popup -->
    </section>

    <section class="mv-50">
      <header>
        <h2>Component : Table </h2>
      </header>
      <div class="table-responsive-md">
        <table class="table table-striped table-bordered">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Handle</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Larry</td>
              <td>the Bird</td>
              <td>@twitter</td>
            </tr>
          </tbody>
        </table><!-- /.table -->
      </div><!-- /.table-responsive-md -->
    </section>
  </div><!-- /.container -->
</main><!-- /#main-->

<?php include('include/footer.php') ?>
